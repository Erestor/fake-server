const ApiLoader = require('./services/apiloader').ApiLoader;
const bodyParser = require('body-parser');
const cors = require('cors');
const express = require('express');
const app = express();
const apiLoader = new ApiLoader(app);

app.use(bodyParser.json());
app.use(cors());

app.post('/api/addnewapi', (req,res) => {
    let apiDefinition = req.body;
    apiLoader.loadNewApi(apiDefinition);
    res.status(200).send("ok");
});

app.use(express.static(__dirname + '/client/app'));

app.listen(55555, () => console.log('app listening on port 55555!'));
