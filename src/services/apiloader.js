const ResponseSearcher = require('./responseSearcher').ResponseSearcher;

class ApiLoader {
    constructor(app) {
        this.app = app;
    }

    loadNewApi(apiDefinition){
        this.app.get(apiDefinition.endPoint, (req, res) => {
            let searcher = new ResponseSearcher();
            if(typeof apiDefinition.param !== 'undefined'){
                let query = req.query;
                let response = searcher.searchResponse(query,apiDefinition);
                if(response !== null){
                    res.send(response);
                }else{
                    res.send({"fake-server":"error"});
                }
            }else{
                res.send(apiDefinition.response);
            }
        });
    }
}

exports.ApiLoader = ApiLoader;
