class ResponseSearcher {
    searchResponse(query,param){
        if(typeof param.response !== 'undefined'){
            return param.response;
        }
        let innerParam = param.param;
        if(typeof innerParam !== 'undefined'){
            return this.searchResponse(query,innerParam);
        }
        let paramKey = Object.keys(param)[0];
        let paramVal = param[paramKey];
        if(query.hasOwnProperty(paramKey)){
            let actualParam = paramVal[query[paramKey]];
            if(typeof actualParam !== 'undefined'){
                return this.searchResponse(query,actualParam);
            }else{
                return this.searchResponse(query,paramVal);
            }
        }
    }
}

exports.ResponseSearcher = ResponseSearcher;
